<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|max:255',
            'text'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'name' => [
                'required' => 'Поле имя обязательно к заполнению',
                'max'=>'максимально допустимое количество символов - :max',
            ],
            'text' => [
                'required' => 'Поле сообщение обязательно к заполнению',
            ],
        ];
    }
}
