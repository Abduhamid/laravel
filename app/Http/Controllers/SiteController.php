<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{
    public function execute(){
        $model=Comment::all();
        return view('site.index', array('models'=>$model));
    }
    public function create(CommentRequest $request){
        $comment=new Comment();
        $comment->name=$request->name;
        $comment->text=$request->text;
        $comment->save();
        return redirect()->route('index');
    }

    public function delete(Request $request){
        $comment=Comment::findOrFail($request->id);
        $comment->delete();
        return redirect()->route('index');
    }
}
