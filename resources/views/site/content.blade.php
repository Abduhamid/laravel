@if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container my-5">
    <section class="dark-grey-text mb-5">
        <h3 class="font-weight-bold text-center mb-5">Отзывы</h3>
            @if(isset($models) && is_object($models))
                @foreach($models as $model)
                <div class="media-body">
                    <div class="float-right">
                        <form method="post" action="{{route('delete')}}">
                            @csrf
                            <input type="hidden" name="id" value={{$model->id}}>
                            <button class="btn"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </div>
                    <h5 class="user-name font-weight-bold">{{$model->name}}</h5>
                    <div class="card-data">
                        <ul class="list-unstyled mb-1">
                            <li class="comment-date font-small grey-text">
                            <i class="far fa-clock"></i> {{$model->created_at}}</li>
                        </ul>
                    </div>
                    <p class="dark-grey-text article">{{$model->text}}</p>
                </div>
                <hr>
            @endforeach
        @endif
    </section>
</div>
<div class="container my-5 py-5 ">
    <section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <form class="text-center" method="post" action="{{route('indexs')}}">
                    @csrf
                    <h3 class="font-weight-bold mb-4">Отзыв</h3>
                    <input type="text" name="name" id="defaultContactFormName" class="form-control mb-4" placeholder="Имя">
                    <div class="form-group">
				    <textarea class="form-control rounded-0" name="text" id="exampleFormControlTextarea2" rows="3"
                            placeholder="Введите сообщение"></textarea>
                    </div>
                    <button class="btn btn-info btn-block" type="submit">Отправить</button>
                </form>
            </div>
        </div>
    </section>
</div>