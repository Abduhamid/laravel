<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            ['name'=>'Test Test',
                'text'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Cupiditate magni possimus ducimus provident minima culpa, exercitationem, maxime ad in ifd dfd df necessitatibus libero obcaecati
                natus itaque voluptates ratione deserunt ipsum et impedit.',
                'created_at'=>date('y:m:d'),
            ],
            ['name'=>'Test1 Test1',
                'text'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Cupiditate magni possimus ducimus provident minima culpa, exercitationem, maxime ad in ifd dfd df necessitatibus libero obcaecati
                natus itaque voluptates ratione deserunt ipsum et impedit.',
                'created_at'=>date('y:m:d'),
            ],
        ]);
    }
}
